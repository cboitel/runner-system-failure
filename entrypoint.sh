#!/bin/sh


case "${DEBUG_LENGTH:-0}" in
0|-*)
    ;;
*)
    printf "$DEBUG_LENGTH-%.0s" $(seq 1 $DEBUG_LENGTH ) | cut -c 1-${DEBUG_LENGTH} | tr -d '\n' >&2
    ;;
esac
exec "$@"