# runner-system-failure

This repository demonstrates a bug found in Gitlab runner when:
1. runner is using `FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR` feature enabled (prevents runner from using a non-secure umask `000`)
2. image used to run jobs (defined in `gitlab-ci.yml`) outputs on stderr more than 1024 bytes of data from within its entrypoint

## Image used

[`Dockerfile`](/Dockerfile) from this repository was used to build an image:
- based on `alpine:latest`
- uses [`entrypoint.sh`](./entrypoint.sh) that displays as much as `DEBUG_LENGTH` bytes of data on `stderr` before executing any requested command

As a consequence, 

Image is already built and available on Docker Hub so you directly experience its usage:

```shell
# identical behavior
docker run alpine uname
docker run cboitel/gitlab-runner-system-failure uname
# Linux

# ask to execute uname and output exactly DEBUG_LENGTH characters on stderr (no ending newline) 
docker run -e DEBUG_LENGTH=25 cboitel/gitlab-runner-system-failure uname
# 25-25-25-25-25-25-25-25-2Linux

docker run -e DEBUG_LENGTH=25 cboitel/gitlab-runner-system-failure uname 2> /dev/null
# Linux
docker run -e DEBUG_LENGTH=25 cboitel/gitlab-runner-system-failure uname > /dev/null
# 25-25-25-25-25-25-25-25-2

echo uname | docker run -i -e DEBUG_LENGTH=25 cboitel/gitlab-runner-system-failure sh 2> /dev/null
# Linux
echo uname | docker run -i -e DEBUG_LENGTH=25 cboitel/gitlab-runner-system-failure sh > /dev/null
# 25-25-25-25-25-25-25-25-2

# ensure image does not use root
docker run -i -e DEBUG_LENGTH=25 cboitel/gitlab-runner-system-failure id
# uid=65534(nobody) gid=65534(nobody) groups=65534(nobody)
```

## CI tests

We will be using that image inside our Gitlab CI jobs defined in [`gitlab-ci.yml`](.gitlab-ci.yml):
- first stage includes four jobs with `FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR` disabled
- second stage includes the same four jobs with `FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR` *enabled*
- each job asks to run a simple `echo hello` with 512, 1024, 1025 and 2048 character bytes on stderr

Once CI is run, you will find out the following result:
[![CI results](./ci.png)](./ci.png)

Looking at a failing job:
[![CI failing job](./ci-job1025.png)](./ci-job1025.png)
> system failures and error states `id` command ran by runner failed

## Code investigation

Looking at source file [`executors/docker/docker_command.go` from latest version v15.10.1](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/executors/docker/docker_command.go), we can see:
1. runner is checking if it needs to change cloned repository files to some specific owner only if `FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR` is enabled
   > see [`changeFilesOwnership` function](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/executors/docker/docker_command.go#L180)
2. it first looks at user running targeted image
3. if `root` isn't used, it will get user's uid and gid and run a `chown` across the repository cloned files

To grab user's uid/gid, [`getUIDandGID`](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/executors/docker/docker_command.go#L220) is called:
- it uses [`UID`](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/executors/docker/internal/user/user.go#L55) and [`GID`](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/executors/docker/internal/user/user.go#L59) functions inside an internal package
- both functions do use [`executeCommand`](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/executors/docker/internal/user/user.go#L63) which prepares buffers to received stdout/stderr limited to 1024 characters:
  - limitation is implemented in an helper [`limitwriter`](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/helpers/limitwriter/limit_writer.go) which will report an error if you try to write more than the capacity defined
  - actual command execution is performed in [`Exec` internal package function](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.10.1/executors/docker/internal/exec/exec.go) using docker's `stdcopy` package


Issue is that Docker's `stdcopy` package will stop its processing whenever some write fails:
- since our stderr buffer is limited to 1024 characters, an error will be reported
- stdcopy will exit before stdout outputs gets a chance to be copied
- as a result, runner will receive an empty stdout and will consider no output was provided by `id` command