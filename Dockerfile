FROM alpine:latest
USER nobody
COPY ./entrypoint.sh /
ENTRYPOINT [ "/entrypoint.sh"]
